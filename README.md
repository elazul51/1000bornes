# 1000 Bornes

> **This project is a port of the famous French card game 1000 Bornes. It's playable online from 2 to 4 players simultenaously in real time. The goal is to be the first to reach 1000 kilometers by placing distance cards on the game board and sneaking through hazards thrown by your opponents.**

![screen_title](screenshots/screenshot_menu.png)

## Motivation

This project is the second one from my degree at **IFOCOP Paris 11**. It aims at demonstrating my skills in backend JavaScript development. The requirements state that it has to be developed using **HTML5, CSS3 and JavaScript** and to be compatible at least with **Internet Explorer 9, Firefox 5, Chrome 10, Safari 5 and Opera 9**. It is mandatory to make use of NodeJS and we've been given the opportunity to choose between multiple libraries and frameworks to achieve our work.

Personnaly I made the choice to use the following technologies for the **backend** side:

- **JavaScript (ES6)**
- **ExpressJS :** one of the most popular NodeJS framework so far
- **MongoDB :** one of the main NoSQL databases
- **Mongoose :** an Object Data Modeling (ODM) library for MongoDB and NodeJS, allowing to manage relationships and schemas easily
- **Swagger :** for documenting and testing the game API
- **Socket.io :** a framework to handle WebSocket communications and allowing real time data transfer between server and clients
- **Winston :** a logging library used with Express
- **Morgan :** another logger used with Express
- **Dotenv :** a package to get environment configuration from .env file
- **Foreman :** a Procfile manager tool for deploying on Heroku instance
- **SASS :** for CSS pre-processing
- **Nodemon :** for auto-restarting the server on updates

And for the **frontend**:

- **JavaScript (ES6) :** made possible with use of Babel and polyfills for old browsers support
- **AngularJS 1.7.2 :** one of the latest versions of AngularJS that benefits from the concept of components (like Angular 2+)
- **ParcelJS :** a user-friendly web app bundler that comes with no configuration unlike Webpack
- **Babel :** an efficient transpiler which allows coding with ES6 syntax on the frontend side
- **HTML5 / CSS3 :** CSS generated from SASS, no HTML template engine was used
- **Bootstrap :** one of the most famous responsive web framework that helps making the app compatible with both mobile and desktop devices
- **Socket.io client :** the client library that comes with the server-side Socket.io library
- **Moment :** a useful library allowing Date object formatting and manipulations
- **Card-Deck :** a library used for card deck shuffling and card draw, from K.Adam White (https://www.npmjs.com/package/card-deck)
- **AngularJS drag and drop directive :** from Marcel Juenemann (https://github.com/marceljuenemann/angular-drag-and-drop-lists)
- **Mobile drag and drop support :** from Tim Ruffles (https://github.com/timruffles/mobile-drag-drop)

Other **useful tools**:

- **ESLint :** provides hints for better coding style (AirBnB guidelines are used for this project)
- **Prettier :** auto-format the code based on provided ESLint rules

**Hosting**:

- **Heroku :** for free web app hosting (https://www.heroku.com/)
- **mLab :** for free MongoDB database hosting (https://mlab.com)

**API documentation** is available here : http://1000bornes.romain-joly.com/api-docs/

## History

1000 Bornes is a classic French game designed in 1947 by Edmond Dujardin, a driving school material editor from Arcachon, and inspired from the American game Touring created in 1906. After being awarded a silver medal at the prestigious Concours Lépine, the game quickly becomes one of the most famous French games. In 1954, Dujardin enhances the initial concept by creating a card game replacing the game board. The goal is rather simple: be the first to reach 1000 kilometers ("bornes" in French slang). Those 1000 kilometers correspond to the total distance of the National Road 7 known as the "vacations road" which is paved with milestones indicating the number of kilometers like most of the roads in the country.

## Gameplay

![screen_game](screenshots/screenshot_game.png)

### Startup

When accessing the application, users have the possibility to either create a new game or to join an existing one with players already awaiting. In either case, users have to select their avatar and to type in and confirm their name.

Every player is given 6 cards at game start that are hidden to opponents.

Once all players have joined the game, the first one starts by picking a card from the deck to get 7 cards in hand, and then places a card on the game board depending on the actions allowed by the rules.

The player has actually 4 possibilities:

- starting the race by placing a Green Light card on his own battle field
- placing a safety card and playing again by picking a new card
- placing a speed limit on an opponent even if he didn't start the game yet
- if no above-mentioned move is possible, the player can just drop a card to the discard heap

After first player turn ends, then each player plays one after the other following the same rules unless one of them performs a "coup fourré" after being attacked by an opponent.

The ultimate goal is to be the first to reach exactly 1000 kilometers (bornes) by placing distance cards and avoiding hazards.

If no more cards are available in the draw deck, the game continues until no more move is possible, and the player with the highest score is declared winner.

### Game rules

- [1000 Bornes official rules 1](client/assets/data/MB Luxe.pdf)
- [1000 Bornes official rules 2](client/assets/data/rdj-mb-classique-poche-et-pegbo---ok.pdf)

Please note that classic rules are applied for this project, no variant is allowed.

There are **106 cards** in total, dispatched as follows:

#### Distance cards

- ![25km](screenshots/MB-25.png) **25km _(x10)_ :** make the user move 25km forward
- ![50km](screenshots/MB-50.png) **50km _(x10)_ :** make the user move 50km forward
- ![75km](screenshots/MB-75.png) **75km _(x10)_ :** make the user move 75km forward
- ![100km](screenshots/MB-100.png) **100km _(x12)_ :** make the user move 100km forward
- ![200km](screenshots/MB-200.png) **200km _(x4)_ :** make the user move 200km forward

- you can't place more than two 200km on your game board
- if an opponent placed a 50km speed limitation on your board, you can't place 75km, 100km and 200km
- you must reach exactly 1000km to win the game (not more)

#### Hazard cards

- ![crash](screenshots/MB-crash.png) **Car Crash _(x3)_ :** place this card on your opponents battle field to crash their car and prevent them from moving forward.
- ![empty](screenshots/MB-empty.png) **Empty Tank _(x3)_ :** place this card on your opponents battle field to make their car run out of gas and prevent them from moving forward.
- ![flat](screenshots/MB-flat.png) **Flat Tire _(x3)_ :** place this card on your opponents battle field to get a flat tire on their car and prevent them from moving forward.
- ![stop](screenshots/MB-stop.png) **Stop (Red Light) _(x5)_ :** place this card on your opponents battle field to stop their car and prevent them from moving forward.
- ![limit](screenshots/MB-limit.png) **Speed Limit _(x4)_ :** place this card on your opponents battle field to slow them down and prevent them from placing 75km, 100km and 200km distance cards.

- Please note that you can attack an opponent with a speed limit even if he's already been damaged by another attack. For that, you can place the speed limit aside the battle field.

#### Remedy cards

- ![repair](screenshots/MB-repair.png) **Repair _(x6)_ :** place this card on your own battle field in case your car has been crashed in order to repair it.
- ![gas](screenshots/MB-gas.png) **Gas _(x6)_ :** place this card on your own battle field in case your car has run out of gas in order to refill it.
- ![spare](screenshots/MB-spare.png) **Spare Tire _(x6)_ :** place this card on your own battle field in case your car has a flat tire in order to replace it.
- ![roll](screenshots/MB-roll.png) **Roll (Green Light) _(x14)_ :** place this card on your own battle field in case your car has been stopped in order to restart it.
- ![unlimited](screenshots/MB-unlimited.png) **Speed Limit _(x6)_ :** place this card on your own speed cards heap to end a speed limit and allow placing 75km, 100km and 200km distance cards again.

- Please note that regarding battles (red light, car crash, flat tire, empty tank), the game applies the classic game rule which is to wait for placing a green light after a remedy card to allow player to use distance cards again.

#### Safety cards

- ![emergency](screenshots/MB-emergency.png) **Emergency Vehicle _(x1)_ :** once a player has placed the Emergency Vehicle card, opponents can't attack him with Stop and Speed Limit cards. The other advantage is that Green Light cards are no longer required for restarting after recovering from an attack.
- ![ace](screenshots/MB-ace.png) **Driving Ace _(x1)_ :** once a player has placed the Driving Ace card, opponents can't attack him with a Car Crash card.
- ![sealant](screenshots/MB-sealant.png) **Tire Sealant _(x1)_ :** once a player has placed the Tire Sealant card, opponents can't attack him with a Flat Tire card.
- ![tanker](screenshots/MB-tanker.png) **Tanker _(x1)_ :** once a player has placed the Tanker card, opponents can't attack him with an Empty Tank card.

- A safety card can be played as a preventive defense or as a "coup-fourré" in response to an attack.
- In case of a preventive defense, you score only 100km by placed safety card
- In case of a "coup-fourré" responding to an attack, you score 100km + 100km bonus : 200km in total
- In both cases you play again after placing your safety card

#### Coup-fourré

What we call a "coup-fourré" is a special move performed right after being attacked by an opponent. If you own one of the 4 safety cards (Emergency Vehicle, Driving Ace, Tire Sealant, or Tanker) you can immediatly counter-attack with the card that corresponds to the type of attack you've just been strucked by. For instance:

- Emergency Vehicle for Red Light or Speed Limit
- Driving Ace for Car Crash
- Tire Sealant for Flat Tire
- Tanker for Empty Tank

In case of "coup-fourré":

- you avoid the previous opponent attack and prevent future attacks of same kind
- you score 200 points: 100 points for the safety card and another 100 points for the "coup-fourré" itself, please note we don't follow the official rules here, because normally you should score 400 points for a "coup-fourré" but this rule applies only for the 5000 points game variant and this would give a decisive advantage in a simple classic game where the goal is only to reach 1000km (here 1km = 1 point).
- you play again and the other players between the attacker and you miss their turn


## References and resources

- AngularJS drag and drop advanced demo : http://marceljuenemann.github.io/angular-drag-and-drop-lists/demo/#/advanced
- Setup an API with Mongo, Mongoose and Node : https://www.frugalprototype.com/api-mongodb-mongoose-node-js/
- Document a Node based API with Swagger : https://blog.cloudboost.io/adding-swagger-to-existing-node-js-project-92a6624b855b
- Socket.io integration with Express : https://github.com/onedesign/express-socketio-tutorial
- AngularJS components and lifecyle hooks : https://blog.thoughtram.io/angularjs/2016/03/29/exploring-angular-1.5-lifecycle-hooks.html
- Logging with Node : http://tostring.it/2014/06/23/advanced-logging-with-nodejs/

### Graphic resources and tools

- 1000 Bornes SVG card deck : https://boardgamegeek.com/file/download/pjeju99p1f/MB-svg.zip
- Mario Kart avatars : https://www.spriters-resource.com/nintendo_switch/mariokart8deluxe/sheet/94161/
- Mario Kart avatars for scores : https://i.pinimg.com/originals/0e/83/1b/0e831b0dabbbc5b1dae405d9c7626955.png
- French road signs fonts : https://fr.ffonts.net/Caracteres-L4.font.download
- Bootstrap components for AngularJS : http://angular-ui.github.io/bootstrap/

## Acknowledgements

I'd like to thank all the staff from IFOCOP and my colleagues from the DIWJS08 promotion for their advices and support.

## Contact

Romain Joly – [@Rom1_Joly](https://twitter.com/rom1_joly) – [contact@romain-joly.com](mailto:contact@romain-joly.com)

## Contributing

1.  Fork it (<https://gitlab.com/elazul51/cv_game/forks/new>)
2.  Create your feature branch (`git checkout -b feature/fooBar`)
3.  Commit your changes (`git commit -am 'Add some fooBar'`)
4.  Push to the branch (`git push origin feature/fooBar`)
5.  Create a new Pull Request
