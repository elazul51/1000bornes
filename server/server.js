require('dotenv').config();
const logger = require('./utils/logger/appLogger');
const morgan = require('morgan'); // Express middleware for logging
const DbConnection = require('./utils/db/connect');
const express = require('express');
const path = require('path');
const cors = require('cors'); // Express middleware for handling CORS
const swaggerUi = require('swagger-ui-express'); // Express middleware for API documentation
const swaggerSpec = require('./utils/api-doc/swagger');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');
const games = require('./routes/gamesRoute');
const players = require('./routes/playersRoute');
const index = require('./routes/indexRoute');

const { PORT } = process.env;

DbConnection();

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const socketEvents = require('./utils/sockets/events')(io);

server.listen(PORT);
logger.info(`Server running on ${PORT}`);

io.use(socketEvents);
app.use((req, res, next) => {
  res.io = io;
  next();
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(path.join(__dirname, '..', 'client', 'favicon.ico')));
app.use(express.static(path.join(__dirname, '..', 'client', 'dist')));

// Server logs
app.use(morgan('dev', {
  skip(req, res) {
    return res.statusCode < 400;
  },
  stream: process.stderr,
}));
app.use(morgan('dev', {
  skip(req, res) {
    return res.statusCode >= 400;
  },
  stream: process.stdout,
}));

app.get('/api-docs.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/api/v1', games);
app.use('/api/v1', players);
app.use('/', index);
