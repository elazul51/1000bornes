const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

// prettier-ignore
const PlayerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
      unique: true,
      minlength: [2, 'Name must be at least 2 characters.'],
      maxlength: [30, 'Name must be less than 30 characters.'],
    },
    avatar: {
      type: String,
      required: true,
    },
    number: {
      type: Number,
      validate: {
        validator: Number.isInteger,
        message: '{VALUE} is not an integer value',
      },
    },
    cards: {
      type: Object,
    },
    turn: {
      type: Boolean,
      default: false,
    },
    allowCoupFourre: {
      type: Boolean,
      default: false,
    },
    timeElapsed: {
      type: Number,
      default: 0,
      validate: {
        validator: Number.isInteger,
        message: '{VALUE} is not an integer value',
      },
    },
    score: {
      type: Number,
      default: 0,
      validate: {
        validator: Number.isInteger,
        message: '{VALUE} is not an integer value',
      },
    },
  },
  { timestamps: true } // eslint-disable-line comma-dangle
);

PlayerSchema.plugin(uniqueValidator, { message: '{VALUE} already exists!' });

/**
 * @swagger
 * definitions:
 *   Player:
 *     properties:
 *       name:
 *         type: string
 *       avatar:
 *         type: string
 *       number:
 *         type: integer
 *       cards:
 *         type: object
 *         properties:
 *           hand:
 *             type: array
 *           km25:
 *             type: array
 *           km50:
 *             type: array
 *           km75:
 *             type: array
 *           km100:
 *             type: array
 *           km200:
 *             type: array
 *           speed:
 *             type: array
 *           battle:
 *             type: array
 *           safety:
 *             type: array
 *       turn:
 *         type: boolean
 *       allowCoupFourre:
 *         type: boolean
 *       timeElapsed:
 *         type: integer
 *       score:
 *         type: integer
 */
const PlayersModel = mongoose.model('Player', PlayerSchema);

PlayersModel.getAll = () => PlayersModel.find();

PlayersModel.getById = playerId => PlayersModel.findById(playerId);

PlayersModel.getByName = playerName => PlayersModel.findOne({ name: playerName });

PlayersModel.add = playerToAdd => playerToAdd.save();

PlayersModel.update = (playerId, playerData) =>
  PlayersModel.findByIdAndUpdate(playerId, playerData, { new: true });

PlayersModel.delete = playerId => PlayersModel.findByIdAndDelete(playerId);

module.exports = PlayersModel;
