const mongoose = require('mongoose');

// prettier-ignore
const GameSchema = new mongoose.Schema(
  {
    active: {
      type: Boolean,
      default: true,
      required: true,
    },
    started: {
      type: Boolean,
      default: false,
      required: true,
    },
    nbPlayer: {
      type: Number,
      required: true,
      validate: {
        validator: Number.isInteger,
        message: '{VALUE} is not an integer value',
      },
    },
    players: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Player',
      required: true,
    }],
    cards: {
      type: Object,
    },
    winner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Player',
    },
    timeElapsed: {
      type: Number,
      validate: {
        validator: Number.isInteger,
        message: '{VALUE} is not an integer value',
      },
    },
  },
  { timestamps: true } // eslint-disable-line comma-dangle
);

/**
 * @swagger
 * definitions:
 *   Game:
 *     properties:
 *       active:
 *         type: boolean
 *       started:
 *         type: boolean
 *       nbPlayer:
 *         type: integer
 *       players:
 *         type: array
 *         items:
 *           type: string
 *           description: ObjectId
 *       cards:
 *         type: array
 *       winner:
 *         type: string
 *         description: ObjectId
 *       timeElapsed:
 *         type: integer
 */
const GameModel = mongoose.model('Game', GameSchema);

GameModel.getAll = () => GameModel.find({ active: true, started: false }).populate('players');

GameModel.getById = gameId =>
  GameModel.findOne({ _id: gameId, active: true, started: false }).populate('players');

GameModel.add = gameToAdd =>
  gameToAdd.save().then(addedGame => addedGame.populate('players').execPopulate());

GameModel.update = (gameId, gameData) =>
  GameModel.findByIdAndUpdate(gameId, gameData, { new: true }).then(updatedGame =>
    updatedGame.populate('players').execPopulate());

GameModel.delete = gameId => GameModel.findByIdAndDelete(gameId);

module.exports = GameModel;
