const express = require('express');
const indexController = require('../controllers/indexController');

const router = express.Router();

router.get('/', (req, res) => {
  indexController.get(req, res);
});

router.get('*', (req, res) => {
  indexController.error(req, res);
});

module.exports = router;
