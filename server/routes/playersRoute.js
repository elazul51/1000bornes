const express = require('express');
const playersController = require('../controllers/playersController');

const router = express.Router();

const { API_KEY } = process.env;

const checkApiKey = (req, res, next) => {
  if (req.header('X-API-Key') !== API_KEY) {
    return res.status(401).json({ status: 'Authentication error, please provide valid API key' });
  }
  return next();
};

router.all('/*', checkApiKey);

/**
 * @swagger
 * /api/v1/players:
 *   get:
 *     description: Returns all players
 *     tags:
 *      - Players
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: An array of players
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Player'
 */
router.get('/players', (...args) => playersController.getAll(...args));

/**
 * @swagger
 * /api/v1/players:
 *   post:
 *     description: Creates a new player
 *     tags:
 *       - Players
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/player'
 *     responses:
 *       200:
 *         description: Created player
 *         schema:
 *           $ref: '#/definitions/Player'
 */
router.post('/players', (...args) => playersController.add(...args));

/**
 * @swagger
 * /api/v1/players/{playerId}:
 *   get:
 *     tags:
 *       - Players
 *     description: Returns a single player by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/playerId'
 *     responses:
 *       200:
 *         description: A single player
 *         schema:
 *           $ref: '#/definitions/Player'
 */
router.get('/players/:playerId', (...args) => playersController.getById(...args));

/**
 * @swagger
 * /api/v1/players/name/{playerName}:
 *   get:
 *     tags:
 *       - Players
 *     description: Returns a single player by name
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/playerName'
 *     responses:
 *       200:
 *         description: A single player
 *         schema:
 *           $ref: '#/definitions/Player'
 */
router.get('/players/name/:playerName', (...args) => playersController.getByName(...args));

/**
 * @swagger
 * /api/v1/players/{playerId}:
 *   put:
 *     tags:
 *       - Players
 *     description: Updates a single player
 *     produces: application/json
 *     parameters:
 *       - $ref: '#/parameters/playerId'
 *       - $ref: '#/parameters/player'
 *     responses:
 *       200:
 *         description: Updated player
 *         schema:
 *           $ref: '#/definitions/Player'
 */
router.put('/players/:playerId', (...args) => playersController.update(...args));

/**
 * @swagger
 * /api/v1/players/{playerId}:
 *   delete:
 *     tags:
 *       - Players
 *     description: Deletes a single player
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/playerId'
 *     responses:
 *       200:
 *         type: string
 *         description: Deleted player ObjectId
 */
router.delete('/players/:playerId', (...args) => playersController.delete(...args));

// router.param('playerId', getByIdPlayer);

module.exports = router;
