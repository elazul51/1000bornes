const express = require('express');
const gamesController = require('../controllers/gamesController');

const router = express.Router();

const { API_KEY } = process.env;

const checkApiKey = (req, res, next) => {
  if (req.header('X-API-Key') !== API_KEY) {
    return res.status(401).json({ status: 'Authentication error, please provide valid API key' });
  }
  return next();
};

router.all('/*', checkApiKey);

/**
 * @swagger
 * /api/v1/games:
 *   get:
 *     description: Returns all games
 *     tags:
 *      - Games
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: An array of games
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Game'
 */
router.get('/games', (...args) => gamesController.getAll(...args));

/**
 * @swagger
 * /api/v1/games:
 *   post:
 *     description: Creates a new game
 *     tags:
 *       - Games
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/game'
 *     responses:
 *       200:
 *         description: Created game
 *         schema:
 *           $ref: '#/definitions/Game'
 */
router.post('/games', (...args) => gamesController.add(...args));

/**
 * @swagger
 * /api/v1/games/{gameId}:
 *   get:
 *     tags:
 *       - Games
 *     description: Returns a single game by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/gameId'
 *     responses:
 *       200:
 *         description: A single game
 *         schema:
 *           $ref: '#/definitions/Game'
 */
router.get('/games/:gameId', (...args) => gamesController.getById(...args));

/**
 * @swagger
 * /api/v1/games/{gameId}:
 *   put:
 *     tags:
 *       - Games
 *     description: Updates a single game
 *     produces: application/json
 *     parameters:
 *       - $ref: '#/parameters/gameId'
 *       - $ref: '#/parameters/game'
 *     responses:
 *       200:
 *         description: Updated game
 *         schema:
 *           $ref: '#/definitions/Game'
 */
router.put('/games/:gameId', (...args) => gamesController.update(...args));

/**
 * @swagger
 * /api/v1/games/{gameId}:
 *   delete:
 *     tags:
 *       - Games
 *     description: Deletes a single game
 *     produces:
 *       - application/json
 *     parameters:
 *       - $ref: '#/parameters/gameId'
 *     responses:
 *       200:
 *         type: string
 *         description: Deleted game ObjectId
 */
router.delete('/games/:gameId', (...args) => gamesController.delete(...args));

module.exports = router;
