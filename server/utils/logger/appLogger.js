require('dotenv').config();
const winston = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');

const {
  LOG_FILE_DIR, LOG_FILE_NAME, LOG_ERROR_FILE_NAME, LOG_LEVEL,
} = process.env;

if (!fs.existsSync(LOG_FILE_DIR)) {
  fs.mkdirSync(LOG_FILE_DIR);
}

const logger = new winston.Logger({
  level: LOG_LEVEL || 'info',
  json: true,
  transports: [
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      humanReadableUnhandledException: true,
      json: false,
      colorize: true,
    }),
    new winston.transports.DailyRotateFile({
      name: 'log.combined',
      filename: `%DATE%_${LOG_FILE_NAME}`,
      dirname: LOG_FILE_DIR,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      maxsize: '5m',
      maxFiles: '7d',
      datePattern: 'YYYY-MM-DD',
      colorize: false,
    }),
    new winston.transports.DailyRotateFile({
      name: 'log.error',
      level: 'error',
      filename: `%DATE%_${LOG_ERROR_FILE_NAME}`,
      dirname: LOG_FILE_DIR,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      maxsize: '5m',
      maxFiles: '7d',
      datePattern: 'YYYY-MM-DD',
      colorize: false,
    }),
  ],
  exitOnError: false,
});

module.exports = logger;
