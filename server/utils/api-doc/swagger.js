const swaggerJSDoc = require('swagger-jsdoc');

const { HOST } = process.env;

// Swagger definition
const swaggerDefinition = {
  info: {
    title: '1000 Bornes',
    version: '1.0.0',
    description: 'A simple API for 1000 Bornes card game',
  },
  host: HOST,
  basePath: '/',
  securityDefinitions: {
    ApiKeyAuth: {
      type: 'apiKey',
      in: 'header',
      name: 'X-API-Key',
    },
  },
  security: [
    {
      ApiKeyAuth: [],
    },
  ],
};

// Options for the swagger docs
const options = {
  swaggerDefinition,
  // Path to the API docs
  apis: ['./server/routes/*.js', './server/models/*.js', './server/routes/parameters.yaml'],
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
module.exports = swaggerJSDoc(options);
