require('dotenv').config();
const Mongoose = require('mongoose');
const logger = require('../logger/appLogger');

const { DB_URL } = process.env;

Mongoose.Promise = global.Promise;

const DbConnection = async () => {
  try {
    await Mongoose.connect(DB_URL);
    logger.info('MongoDB connection OK');
  } catch (err) {
    logger.error(`MongoDB connection error: ${err}`);
  }
};

module.exports = DbConnection;
