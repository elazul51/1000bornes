const logger = require('../logger/appLogger');

const sockets = [];
const socketEvents = io => (socket, next) => {
  io.emit('connected', 'Vous êtes connecté au serveur de jeu');

  sockets[socket.id] = socket;
  logger.info(`${sockets.length} connexions actives`);

  socket.on('sync-cards', (cards) => {
    io.emit('cards:save', cards);
  });

  socket.on('sync-players', (player) => {
    io.emit('players:save', player);
  });

  socket.on('sync-game', (game) => {
    io.emit('game:save', game);
  });

  socket.on('sync-start-timer', (start) => {
    io.emit('start-timer', start);
  });

  socket.on('sync-stop-timer', (stop) => {
    io.emit('stop-timer', stop);
  });

  socket.on('send-winner', (player, highestScore) => {
    io.emit('announce-victory', player, highestScore);
  });

  socket.on('send-message', (player, card) => {
    io.emit('display-message', player, card);
  });

  socket.on('disconnect', () => {
    sockets.splice(sockets.indexOf(socket), 1);
  });

  return next();
};

module.exports = socketEvents;
