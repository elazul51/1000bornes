const logger = require('../utils/logger/appLogger');
const Game = require('../models/gamesModel');

const gamesController = {};

gamesController.getAll = async (req, res) => {
  try {
    const games = await Game.getAll();
    logger.info('Getting all active games...');
    res.send(games);
  } catch (err) {
    const message = `Error in getting active games- ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

gamesController.getById = async (req, res) => {
  const { gameId } = req.params;
  try {
    const game = await Game.getById(gameId);
    logger.info(`Getting active game by id ${gameId}...`);
    res.send(game);
  } catch (err) {
    const message = `Error in getting active game by id ${gameId} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

gamesController.add = async (req, res) => {
  const gameToAdd = new Game(req.body);
  try {
    const addedGame = await Game.add(gameToAdd);
    logger.info('Adding game...', addedGame);
    res.send(addedGame);
    res.io.emit('games:save', addedGame);
  } catch (err) {
    const message = `Error in adding game : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

gamesController.update = async (req, res) => {
  const { gameId } = req.params;
  const gameData = req.body;
  try {
    const updatedGame = await Game.update(gameId, gameData);
    logger.info(`Updating game ${gameId}...`, updatedGame);
    res.send(updatedGame);
    res.io.emit('games:save', updatedGame);
  } catch (err) {
    const message = `Error in updating game ${gameId} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

gamesController.delete = async (req, res) => {
  const { gameId } = req.params;
  try {
    const removedGame = await Game.delete(gameId);
    logger.info(`Deleting game ${gameId}...`);
    res.send(gameId);
    res.io.emit('games:remove', removedGame);
  } catch (err) {
    const message = `Error in deleting game ${gameId} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

module.exports = gamesController;
