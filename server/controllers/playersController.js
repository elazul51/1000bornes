const logger = require('../utils/logger/appLogger');
const Player = require('../models/playersModel');

const playersController = {};

playersController.getAll = async (req, res) => {
  try {
    const players = await Player.getAll();
    logger.info('Getting all players...');
    res.send(players);
  } catch (err) {
    const message = `Error in getting players : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

playersController.getById = async (req, res) => {
  const { playerId } = req.params;
  try {
    const player = await Player.getById(playerId);
    logger.info(`Getting player by id ${playerId}...`);
    res.send(player);
  } catch (err) {
    const message = `Error in getting player by id ${playerId} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

playersController.getByName = async (req, res) => {
  const { playerName } = req.params;
  try {
    const player = await Player.getByName(playerName);
    logger.info(`Getting player by name ${playerName}...`);
    res.send(player);
  } catch (err) {
    const message = `Error in getting player by name ${playerName} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

playersController.add = async (req, res) => {
  const playerToAdd = new Player(req.body);
  try {
    const addedPlayer = await Player.add(playerToAdd);
    logger.info('Adding player...', addedPlayer);
    res.send(addedPlayer);
    res.io.emit('players:save', addedPlayer);
  } catch (err) {
    const message = `Error in adding player : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

playersController.update = async (req, res) => {
  const { playerId } = req.params;
  const playerData = req.body;
  try {
    const updatedPlayer = await Player.update(playerId, playerData);
    logger.info(`Updating player ${playerId}...`, updatedPlayer);
    res.send(updatedPlayer);
    res.io.emit('players:save', updatedPlayer);
  } catch (err) {
    const message = `Error in updating player ${playerId} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

playersController.delete = async (req, res) => {
  const { playerId } = req.params;
  try {
    const removedPlayer = await Player.delete(playerId);
    logger.info(`Deleting player ${playerId}...`);
    res.send(playerId);
    res.io.emit('players:remove', removedPlayer);
  } catch (err) {
    const message = `Error in deleting player ${playerId} : ${err}`;
    logger.error(message);
    res.status(400).send(message);
  }
};

module.exports = playersController;
