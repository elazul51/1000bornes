const logger = require('../utils/logger/appLogger');
const path = require('path');

const controller = {};

controller.get = (req, res) => {
  try {
    res.sendFile(path.join(__dirname, '..', '..', 'client', 'dist', 'index.html'));
  } catch (err) {
    logger.error(`Error in getting index- ${err}`);
    res.send('Got error in get');
  }
};

controller.error = (req, res) => {
  try {
    res.status(404);
    res.sendFile(path.join(__dirname, '..', 'views', '404.html'));
  } catch (err) {
    logger.error(`Error in getting index- ${err}`);
    res.send('Got error in error 404');
  }
};

module.exports = controller;
