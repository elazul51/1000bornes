import 'babel-polyfill';
import { polyfill } from 'mobile-drag-drop';
// optional import of scroll behaviour
import { scrollBehaviourDragImageTranslateOverride } from 'mobile-drag-drop/scroll-behaviour';
import angular from 'angular';
import accordion from 'angular-ui-bootstrap/src/accordion';
import buttons from 'angular-ui-bootstrap/src/buttons';
import progressbar from 'angular-ui-bootstrap/src/progressbar';
import modal from 'angular-ui-bootstrap/src/modal';
import tooltip from 'angular-ui-bootstrap/src/tooltip';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import 'angular-socket-io';
import uiRouter from 'angular-ui-router';
import angularMoment from 'angular-moment';
import 'moment/locale/fr';
import dndLists from 'angular-drag-and-drop-lists';
import routing from './app.routes';
import './app.scss';
import cardService from './components/card/card.service';
import dialogService from './components/dialog/dialog.service';
import socketService from './components/socket/socket.service';
import gameMenu from './components/game-menu/game-menu.component';
import game from './components/game/game.component';

polyfill({
  // use this to make use of the scroll behaviour
  dragImageTranslateOverride: scrollBehaviourDragImageTranslateOverride,
});

// workaround to make scroll prevent work in iOS Safari > 10
try {
  window.addEventListener('touchmove', () => { }, { passive: false });
} catch (e) {
  console.error(e);
}

export default function run($http) {
  $http.defaults.headers.common['X-API-Key'] = '38c79a1d-d0fd-4669-923f-de4747107ebe'; // eslint-disable-line no-param-reassign
}
run.$inject = ['$http'];

angular
  .module('app', [
    ngAnimate,
    ngSanitize,
    accordion,
    buttons,
    progressbar,
    tooltip,
    modal,
    'btford.socket-io',
    cardService,
    dialogService,
    socketService,
    uiRouter,
    angularMoment,
    dndLists,
    gameMenu,
    game,
  ])
  .config(routing)
  .run(run)
  .run((amMoment) => {
    amMoment.changeLocale('fr');
  });
angular.element(document).ready(() => {
  angular.bootstrap(document, ['app'], {
    strictDi: true,
  });
});
