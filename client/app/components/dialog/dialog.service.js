import angular from 'angular';
import templateConfirm from './dialog-confirm.html';
import templateAlert from './dialog-alert.html';
import './dialog.scss';

const DialogService = ($uibModal) => {
  'ngInject';

  return {
    confirm(message, title) {
      const modal = $uibModal.open({
        size: 'sm',
        template: templateConfirm,
        backdrop: 'static',
        keyboard: false,
        controller($scope, $uibModalInstance) {
          $scope.modal = $uibModalInstance;

          if (angular.isObject(message)) {
            angular.extend($scope, message);
          } else {
            $scope.message = message;
            $scope.title = angular.isUndefined(title) ? 'Confirmer' : title;
          }
        },
      });

      return modal.result;
    },

    alert(message, title) {
      const modal = $uibModal.open({
        size: 'sm',
        template: templateAlert,
        backdrop: 'static',
        keyboard: false,
        controller($scope, $uibModalInstance) {
          $scope.modal = $uibModalInstance;
          if (angular.isObject(message)) {
            angular.extend($scope, message);
          } else {
            $scope.message = message;
            $scope.title = angular.isUndefined(title) ? 'Message' : title;
          }
        },
      });

      return modal.result;
    },
  };
};

export default angular.module('app.dialog', []).factory('dialogService', DialogService).name;
