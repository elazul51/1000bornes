import angular from 'angular';
import template from './game.html';
import './game.scss';

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
export class GameComponent {
  static id = 'game';

  allowedCardTypes = [
    'card-km25',
    'card-km50',
    'card-km75',
    'card-km100',
    'card-km200',
    'card-ace',
    'card-crash',
    'card-emergency',
    'card-empty',
    'card-flat',
    'card-gas',
    'card-limit',
    'card-repair',
    'card-roll',
    'card-sealant',
    'card-spare',
    'card-stop',
    'card-tanker',
    'card-unlimited',
  ];

  apiGamesEndpoint = '/api/v1/games';
  apiPlayersEndpoint = '/api/v1/players';

  gameQuitMessage =
    "Attention, vous êtes sur le point d'abandonner la partie. Êtes-vous sûr de vouloir quitter cette page?";
  gameDuration = 0;
  timer;
  winner;

  /* @ngInject */
  constructor($http, $scope, $state, $stateParams, $interval, $window, socketService, cardService, dialogService) { // eslint-disable-line max-len
    this.$http = $http;
    this.$scope = $scope;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$interval = $interval;
    this.socketService = socketService;
    this.cardService = cardService;
    this.dialogService = dialogService;

    $window.addEventListener('beforeunload', (e) => {
      const confirmationMessage = this.gameQuitMessage;

      e.returnValue = confirmationMessage;
      return confirmationMessage;
    });

    $scope.$on('$locationChangeStart', (e) => {
      e.preventDefault();
    });

    $scope.$on('$destroy', () => {
      this.socketService.socket.emit('sync-stop-timer', true);
      socketService.socket.removeAllListeners('start-timer');
      socketService.socket.removeAllListeners('stop-timer');
      socketService.socket.removeAllListeners('announce-victory');
      socketService.socket.removeAllListeners('display-message');
      socketService.socket.removeAllListeners('game:save');
      socketService.socket.removeAllListeners('cards:save');
      socketService.unsyncUpdates('players');
    });
  }

  $onInit() {
    this.game = this.$stateParams.game;
    this.playerId = this.$stateParams.playerId;
    this.deck = this.cardService.initCards(this.game.cards);
    this.socketService.socket.on('cards:save', (cards) => {
      this.$scope.$apply(() => {
        this.game.cards = cards;
        this.deck = this.cardService.initCards(this.game.cards);
      });
    });
    this.socketService.socket.on('game:save', (game) => {
      this.$scope.$apply(() => {
        this.game = game;
        this.deck = this.cardService.initCards(this.game.cards);
        this.socketService.syncUpdates('players', this.game.players);
      });
    });
    this.socketService.socket.on('start-timer', (start) => {
      this.$scope.$apply(() => {
        if (start) {
          this.startTimer();
        }
      });
    });
    this.socketService.socket.on('stop-timer', (stop) => {
      this.$scope.$apply(() => {
        if (stop) {
          this.stopTimer();
        }
      });
    });
    this.socketService.socket.on('announce-victory', (player, highestScore) => {
      if (player._id === this.playerId) {
        this.announceVictory(player, highestScore);
      } else {
        this.announceDefeat(player);
      }
    });
    this.socketService.socket.on('display-message', (player, card) => {
      if (player._id === this.playerId) {
        this.displayMessage(player, card);
      }
    });
  }

  saveGame() {
    for (let i = 0; i < this.game.players; i += 1) {
      const player = this.game.players[i];
      this.$http.put(`${this.apiPlayersEndpoint}/${player._id}`, {
        turn: player.turn,
        allowCoupFourre: player.allowCoupFourre,
        cards: player.cards,
        score: player.score,
        timeElapsed: this.gameDuration,
      })
        .then((response) => {
          if (response.status !== 200) {
            throw new Error('Error: unable to update player');
          }
        })
        .catch((error) => {
          console.error('An error occured while saving players: ', error);
        });
    }

    this.$http.put(`${this.apiGamesEndpoint}/${this.game._id}`, {
      active: false,
      players: this.game.players,
      cards: this.game.cards,
      winner: this.winner,
      timeElapsed: this.gameDuration,
    })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to update game');
        }
      })
      .catch((error) => {
        console.error('An error occured while saving the game: ', error);
      });
  }

  getPlayer(id) {
    return this.game.players.find(el => el._id === id);
  }

  getCurrentPlayer() {
    return this.game.players.find(el => el._id === this.playerId);
  }

  getNextPlayer(currentPlayer) {
    return this.game.players.find(el => el.number === (currentPlayer.number % this.game.players.length) + 1); // eslint-disable-line max-len
  }

  getHighestScorePlayer() {
    return this.game.players.reduce((p1, p2) => (p1.score > p2.score ? p1 : p2));
  }

  stopTimer() {
    this.$interval.cancel(this.timer);
  }

  startTimer() {
    this.stopTimer();
    this.timer = this.$interval(
      () => {
        this.gameDuration += 1000;
      },
      1000,
    );
  }

  mustPickCard(player) {
    return player.turn &&
      player.cards.hand.length < 7 &&
      !player.allowCoupFourre &&
      this.deck.remaining();
  }

  changeTurn(player) {
    // disable current player turn
    const currentPlayer = player;
    currentPlayer.turn = false;
    this.socketService.socket.emit('sync-players', currentPlayer);

    // enable next player turn
    const nextPlayer = this.getNextPlayer(currentPlayer);
    nextPlayer.turn = true;
    this.socketService.socket.emit('sync-players', nextPlayer);

    // end the game if no more card available and get highest score
    if (nextPlayer.cards.hand.length === 0) {
      this.socketService.socket.emit('sync-stop-timer', true);
      this.winner = this.getHighestScorePlayer();
      this.saveGame();
      this.socketService.socket.emit('send-winner', this.winner, true);
    }
  }

  forceTurn(player) {
    for (let i = 0; i < this.game.players.length; i += 1) {
      if (player._id !== this.game.players[i]._id) {
        const otherPlayer = this.game.players[i];
        otherPlayer.turn = false;
        this.socketService.socket.emit('sync-players', otherPlayer);
      }
    }
  }

  movedCallback(event, $index, player) {
    const currentPlayer = player;
    const movedCard = currentPlayer.cards.hand[$index];
    currentPlayer.cards.hand.splice($index, 1);
    this.socketService.socket.emit('sync-players', currentPlayer);

    if (currentPlayer.score === 1000) {
      this.socketService.socket.emit('sync-stop-timer', true);
      this.winner = currentPlayer;
      this.saveGame();
      this.socketService.socket.emit('send-winner', this.winner, true);
    } else if (!(movedCard.rules && movedCard.rules.replay)) {
      this.changeTurn(currentPlayer);
    }
  }

  announceVictory(player, highestScore) {
    const message = highestScore ? `vous avez le score le plus élevé avec ${player.score} bornes` : 'vous avez atteint les 1000 bornes le premier';
    this.dialogService.alert(
      `<span class="fa-stack trophy">
        <span class="fas fa-trophy fa-stack-2x"></span>
        <span class="fa-stack-1x portrait ${player.avatar}"></span>
      </span>
      <div>Félicitations <span class="winner">${player.name}</span>, ${message} et vous remportez la partie face à vos adversaires!</div>`,
      'Victoire !',
    ).then(() => {
      this.$state.go('gameMenu');
    });
  }

  announceDefeat(player) {
    const currentPlayer = this.getCurrentPlayer();
    this.dialogService.alert(
      `<span class="fa-stack loose">
        <span class="fas fa-thumbs-down fa-stack-2x"></span>
        <span class="fa-stack-1x portrait ${currentPlayer.avatar}"></span>
      </span>
      <div>Malheureusement vous avez perdu ${currentPlayer.name}. C'est <span class="winner">${player.name}</span> qui remporte la victoire avant vous. Retentez votre chance une prochaine fois!</div>`,
      'Défaite',
    ).then(() => {
      this.$state.go('gameMenu');
    });
  }

  displayMessage(player, card) {
    let message = '';
    let title = card.label;
    switch (card.type) {
      case 'crash':
        message = 'Vous venez d\'avoir un accident, réparez votre voiture et redémarrez avec un feu vert pour pouvoir continuer la course!';
        break;
      case 'empty':
        message = 'Vous êtes tombé en panne d\'essence, faites le plein et redémarrez avec un feu vert pour pouvoir continuer la course!';
        break;
      case 'flat':
        message = 'Vous avez un pneu crevé, réparez votre pneu et redémarrez avec un feu vert pour pouvoir continuer la course!';
        break;
      case 'stop':
        message = 'Vous êtes arrêté à un feu rouge, redémarrez avec un feu vert pour pouvoir continuer la course!';
        break;
      case 'limit':
        message = 'Vous êtes soumis à une limitation de vitesse, posez une fin de limite de vitesse pour pouvoir de nouveau jouer des distances de 75km, 100km, ou 200km!';
        break;
      case 'emergency':
        title = player.allowCoupFourre && player.allowCoupFourre.safety === 'emergency' ? `Coup-Fourré ${title}` : title;
        message = player.allowCoupFourre && player.allowCoupFourre.safety === 'emergency' ? 'Bravo, vous réalisez un coup-fourré avec le Véhicule Prioritaire!<br>' : '';
        message += 'Vous avez posé le Véhicule Prioritaire, vous ne pouvez plus être attaqué par une Feu Rouge ni une Limitation de Vitesse, et vous êtes dispensé de Feu Vert.';
        break;
      case 'ace':
        title = player.allowCoupFourre && player.allowCoupFourre.safety === 'ace' ? `Coup-Fourré ${title}` : title;
        message = player.allowCoupFourre && player.allowCoupFourre.safety === 'ace' ? 'Bravo, vous réalisez un coup-fourré avec l\'As du Volant!<br>' : '';
        message += 'Vous avez posé l\'As du Volant, vous ne pouvez plus être attaqué par un Accident.';
        break;
      case 'sealant':
        title = player.allowCoupFourre && player.allowCoupFourre.safety === 'sealant' ? `Coup-Fourré ${title}` : title;
        message = player.allowCoupFourre && player.allowCoupFourre.safety === 'sealant' ? 'Bravo, vous réalisez un coup-fourré avec le Pneu Increvable!<br>' : '';
        message += 'Vous avez posé le Pneu Increvable, vous ne pouvez plus être attaqué par une Crevaison.';
        break;
      case 'tanker':
        title = player.allowCoupFourre && player.allowCoupFourre.safety === 'tanker' ? `Coup-Fourré ${title}` : title;
        message = player.allowCoupFourre && player.allowCoupFourre.safety === 'tanker' ? 'Bravo, vous réalisez un coup-fourré avec le Camion Citerne!<br>' : '';
        message += 'Vous avez posé le Camion Citerne, vous ne pouvez plus être attaqué par une Panne d\'Essence.';
        break;
      default:
    }
    if (message) {
      this.dialogService.alert(`<span class="card ${card.type}"></span><div>${message}</div>`, title);
    }
  }

  drawCard(player) {
    // starts game duration timer
    if (!this.gameDuration) {
      this.socketService.socket.emit('sync-start-timer', true);
    }

    // reset previous coup-fourré chance in case player didn't play it
    // before next player picked a card
    this.disableCoupFourre();

    if (player.cards.hand.length < 7 && !player.allowCoupFourre) {
      const currentPlayer = player;
      const newCard = this.cardService.dealCards();
      this.socketService.socket.emit('sync-cards', this.cardService.getCards());
      currentPlayer.cards.hand.push(newCard);
      this.socketService.socket.emit('sync-players', currentPlayer);
      return true;
    }
    return false;
  }

  dropDistance(item, index, type, player) {
    const currentPlayer = player;
    const battleCard = currentPlayer.cards.battle.slice(-1)[0];
    const speedCard = currentPlayer.cards.speed.slice(-1)[0];
    const hasEmergencyCard = GameComponent.hasPlacedSafety(currentPlayer, 'emergency');
    const hasAceCard = GameComponent.hasPlacedSafety(currentPlayer, 'ace');
    const hasTankerCard = GameComponent.hasPlacedSafety(currentPlayer, 'tanker');
    const hasSealantCard = GameComponent.hasPlacedSafety(currentPlayer, 'sealant');
    if (((battleCard && battleCard.type === 'roll') ||
      (hasEmergencyCard &&
        (!battleCard ||
          (battleCard &&
            (battleCard.type === 'stop' ||
              battleCard.type === 'repair' ||
              battleCard.type === 'gas' ||
              battleCard.type === 'spare' ||
              (battleCard.type === 'crash' && hasAceCard) ||
              (battleCard.type === 'empty' && hasTankerCard) ||
              (battleCard.type === 'flat' && hasSealantCard)))))) &&
      item.value + currentPlayer.score <= 1000
    ) {
      if (type === 'card-km25') {
        currentPlayer.cards.km25.push(item);
      } else if (type === 'card-km50') {
        currentPlayer.cards.km50.push(item);
      } else if (hasEmergencyCard || !(speedCard && speedCard.type === 'limit')) {
        if (type === 'card-km75') {
          currentPlayer.cards.km75.push(item);
        } else if (type === 'card-km100') {
          currentPlayer.cards.km100.push(item);
        } else if (type === 'card-km200') {
          if (currentPlayer.cards.km200.length < item.rules.limit) {
            currentPlayer.cards.km200.push(item);
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
      currentPlayer.score += item.value;
      this.socketService.socket.emit('sync-players', currentPlayer);
      return true;
    }
    return false;
  }

  static hasSafety(player, safetyType) {
    return player.cards.hand.find(el => el && el.type && el.type === safetyType);
  }

  static hasPlacedSafety(player, safetyType) {
    return player.cards.safety.find(el => el && el.type && el.type === safetyType);
  }

  static getCoupFourreType(attackType) {
    let coupFourreType;
    switch (attackType) {
      case 'limit':
      case 'stop':
        coupFourreType = 'emergency';
        break;
      case 'crash':
        coupFourreType = 'ace';
        break;
      case 'empty':
        coupFourreType = 'tanker';
        break;
      case 'flat':
        coupFourreType = 'sealant';
        break;
      default:
    }

    return coupFourreType;
  }

  enableCoupFourre(player, card) {
    const targetPlayer = player;
    targetPlayer.allowCoupFourre = {};
    targetPlayer.allowCoupFourre.attack = card.type;
    targetPlayer.allowCoupFourre.safety = GameComponent.getCoupFourreType(card.type);
    this.socketService.socket.emit('sync-players', targetPlayer);
  }

  disableCoupFourre() {
    for (let i = 0; i < this.game.players.length; i += 1) {
      const player = this.game.players[i];
      if (player.allowCoupFourre) {
        player.allowCoupFourre = false;
        this.socketService.socket.emit('sync-players', player);
      }
    }
  }

  dropSafety(item, index, type, player) {
    const currentPlayer = player;

    // you can only play a coup-fourré if it's not your turn
    if (currentPlayer.allowCoupFourre &&
      currentPlayer.allowCoupFourre.safety &&
      currentPlayer.allowCoupFourre.safety !== item.type) {
      return false;
    }

    let i;
    if (index < 1) {
      i = 0;
    } else if (index > currentPlayer.cards.safety.length) {
      i = currentPlayer.cards.safety.length - 1;
    } else {
      i = index - 1;
    }
    if (!currentPlayer.cards.safety[i]) {
      // in case of coup-fourré
      if (currentPlayer.allowCoupFourre &&
        currentPlayer.allowCoupFourre.safety &&
        currentPlayer.allowCoupFourre.safety === item.type) {
        // force turn to current player
        this.forceTurn(currentPlayer);

        // reject last attack to the discarded heap
        let lastAttack;
        if (currentPlayer.allowCoupFourre.attack === 'limit') {
          lastAttack = currentPlayer.cards.speed.pop();
        } else {
          lastAttack = currentPlayer.cards.battle.pop();
        }
        currentPlayer.cards.discarded.push(lastAttack);

        // coup-fourré bonus
        currentPlayer.score += item.value + 100;
      } else {
        currentPlayer.score += item.value;
      }
      currentPlayer.score = currentPlayer.score > 1000 ? 1000 : currentPlayer.score;
      // assign safety and sen notification to user
      currentPlayer.cards.safety[i] = item;
      this.socketService.socket.emit('send-message', currentPlayer, item);

      // replay and disallow coup-fourré
      currentPlayer.turn = true;
      currentPlayer.allowCoupFourre = false;
      this.socketService.socket.emit('sync-players', currentPlayer);
      return true;
    }
    return false;
  }

  dropSpeed(item, index, type, player, targetPlayer) {
    const currentPlayer = player;
    const i = index - 1;
    const previousCard = targetPlayer.cards.speed[i - 1];
    const selfDrop = currentPlayer._id === targetPlayer._id;
    const hasEmergencyCardCurrentPlayer = GameComponent.hasPlacedSafety(currentPlayer, 'emergency');
    const hasEmergencyCardTargetPlayer = GameComponent.hasPlacedSafety(targetPlayer, 'emergency');
    if (
      ((selfDrop &&
        !hasEmergencyCardCurrentPlayer &&
        previousCard && previousCard.type === 'limit' &&
        type === 'card-unlimited') ||
        (!selfDrop &&
          !hasEmergencyCardTargetPlayer &&
          ((!previousCard && type === 'card-limit') ||
            (previousCard && previousCard.type === 'unlimited' && type === 'card-limit'))))
    ) {
      if (selfDrop) {
        currentPlayer.cards.speed.push(item);
        this.socketService.socket.emit('sync-players', currentPlayer);
      } else {
        targetPlayer.cards.speed.push(item);
        this.socketService.socket.emit('sync-players', targetPlayer);
        this.socketService.socket.emit('send-message', targetPlayer, item);
        // test if "coup-fourré" is allowed
        if (item.type === 'limit' && GameComponent.hasSafety(targetPlayer, 'emergency')) {
          this.enableCoupFourre(targetPlayer, item);
        }
      }
      return true;
    }
    return false;
  }

  dropBattle(item, index, type, player, targetPlayer) {
    const currentPlayer = player;
    const i = index - 1;
    const previousCard = targetPlayer.cards.battle[i - 1];
    const selfDrop = currentPlayer._id === targetPlayer._id;
    const hasEmergencyCardCurrentPlayer = GameComponent.hasPlacedSafety(currentPlayer, 'emergency');
    const hasAceCardCurrentPlayer = GameComponent.hasPlacedSafety(currentPlayer, 'ace');
    const hasTankerCardCurrentPlayer = GameComponent.hasPlacedSafety(currentPlayer, 'tanker');
    const hasSealantCardCurrentPlayer = GameComponent.hasPlacedSafety(currentPlayer, 'sealant');
    const hasEmergencyCardTargetPlayer = GameComponent.hasPlacedSafety(targetPlayer, 'emergency');
    const hasAceCardTargetPlayer = GameComponent.hasPlacedSafety(targetPlayer, 'ace');
    const hasTankerCardTargetPlayer = GameComponent.hasPlacedSafety(targetPlayer, 'tanker');
    const hasSealantCardTargetPlayer = GameComponent.hasPlacedSafety(targetPlayer, 'sealant');
    if (
      (selfDrop &&
        ((type === 'card-roll' && !previousCard && !hasEmergencyCardCurrentPlayer) ||
          (previousCard &&
            ((((previousCard.type === 'stop') ||
              (previousCard.type === 'repair' && !hasAceCardCurrentPlayer) ||
              (previousCard.type === 'gas' && !hasTankerCardCurrentPlayer) ||
              (previousCard.type === 'spare' && !hasSealantCardCurrentPlayer)) &&
              !hasEmergencyCardCurrentPlayer &&
              previousCard.type !== 'roll' &&
              type === 'card-roll') ||
              (previousCard.type === 'crash' &&
                ((type === 'card-repair' && !hasAceCardCurrentPlayer) ||
                  (type === 'card-roll' && hasAceCardCurrentPlayer && !hasEmergencyCardCurrentPlayer))) ||
              (previousCard.type === 'empty' &&
                ((type === 'card-gas' && !hasTankerCardCurrentPlayer) ||
                  (type === 'card-roll' && hasTankerCardCurrentPlayer && !hasEmergencyCardCurrentPlayer))) ||
              (previousCard.type === 'flat' &&
                ((type === 'card-spare' && !hasSealantCardCurrentPlayer) ||
                  (type === 'card-roll' && hasSealantCardCurrentPlayer && !hasEmergencyCardCurrentPlayer))))))) ||
      (!selfDrop &&
        (((previousCard && previousCard.type === 'roll') ||
          (hasEmergencyCardTargetPlayer && !previousCard) ||
          (hasEmergencyCardTargetPlayer &&
            previousCard &&
            (previousCard.type === 'roll' ||
              previousCard.type === 'repair' ||
              previousCard.type === 'gas' ||
              previousCard.type === 'spare'))) &&
          ((type === 'card-stop' && !hasEmergencyCardTargetPlayer) ||
            (type === 'card-crash' && !hasAceCardTargetPlayer) ||
            (type === 'card-empty' && !hasTankerCardTargetPlayer) ||
            (type === 'card-flat' && !hasSealantCardTargetPlayer))))
    ) {
      if (selfDrop) {
        currentPlayer.cards.battle.push(item);
        this.socketService.socket.emit('sync-players', currentPlayer);
      } else {
        targetPlayer.cards.battle.push(item);
        this.socketService.socket.emit('sync-players', targetPlayer);
        this.socketService.socket.emit('send-message', targetPlayer, item);
        // test if "coup-fourré" is allowed
        if ((item.type === 'stop' && GameComponent.hasSafety(targetPlayer, 'emergency')) ||
          (item.type === 'crash' && GameComponent.hasSafety(targetPlayer, 'ace')) ||
          (item.type === 'empty' && GameComponent.hasSafety(targetPlayer, 'tanker')) ||
          (item.type === 'flat' && GameComponent.hasSafety(targetPlayer, 'sealant'))) {
          this.enableCoupFourre(targetPlayer, item);
        }
      }
      return true;
    }
    return false;
  }
}

export default angular.module('directives.game', []).component(GameComponent.id, {
  template,
  controller: GameComponent,
}).name;
