import angular from 'angular';
import io from 'socket.io-client';

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
const SocketService = (socketFactory) => {
  'ngInject';

  // socket.io now auto-configures its connection when we ommit a connection url
  const ioSocket = io();

  const socket = socketFactory({
    ioSocket,
  });

  return {
    socket,

    /**
     * Register listeners to sync an array with updates on a model
     *
     * Takes the array we want to sync, the model name that socket updates are sent from,
     * and an optional callback function after new items are updated.
     *
     * @param {String} modelName
     * @param {Array} array
     * @param {Function} cb
     */
    syncUpdates(modelName, array, cb = angular.noop) {
      /**
       * Syncs item creation/updates on 'model:save'
       */
      socket.on(`${modelName}:save`, (item) => {
        let event = 'created';
        const oldItem = array.find(el => el._id === item._id);
        const index = array.indexOf(oldItem);

        // replace oldItem if it exists
        // otherwise just add item to the collection
        if (oldItem) {
          array.splice(index, 1, item);
          event = 'updated';
        } else if (Array.isArray(array)) {
          array.push(item);
        }

        cb(event, item, array);
      });

      /**
       * Syncs removed items on 'model:remove'
       */
      socket.on(`${modelName}:remove`, (item) => {
        const event = 'deleted';
        const newArray = array.filter(el => el._id !== item._id);
        cb(event, item, newArray);
      });
    },

    /**
     * Removes listeners for a models updates on the socket
     *
     * @param modelName
     */
    unsyncUpdates(modelName) {
      socket.removeAllListeners(`${modelName}:save`);
      socket.removeAllListeners(`${modelName}:remove`);
    },
  };
};

export default angular.module('app.socket', []).factory('socketService', SocketService).name;
