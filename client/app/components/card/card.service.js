import angular from 'angular';
import CardDeck from 'card-deck';

/* eslint no-underscore-dangle: ["error", { "allow": ["_stack"] }] */
const CardService = () => {
  'ngInject';

  let cards = [];

  const definitions = [
    {
      category: 'hazard',
      types: [
        {
          name: 'crash',
          label: 'Accident',
          count: 3,
        },
        {
          name: 'empty',
          label: "Panne d'essence",
          count: 3,
        },
        {
          name: 'flat',
          label: 'Crevaison',
          count: 3,
        },
        {
          name: 'stop',
          label: 'Feu rouge',
          count: 5,
        },
        {
          name: 'limit',
          label: 'Limite de vitesse',
          count: 4,
        },
      ],
    },
    {
      category: 'remedy',
      types: [
        {
          name: 'repair',
          label: 'Réparations',
          count: 6,
        },
        {
          name: 'gas',
          label: 'Essence',
          count: 6,
        },
        {
          name: 'spare',
          label: 'Roue de secours',
          count: 6,
        },
        {
          name: 'roll',
          label: 'Feu vert',
          count: 14,
        },
        {
          name: 'unlimited',
          label: 'Fin de limite de vitesse',
          count: 6,
        },
      ],
    },
    {
      category: 'safety',
      types: [
        {
          name: 'ace',
          label: 'As du volant',
          count: 1,
          value: 100,
          rules: {
            replay: true,
          },
        },
        {
          name: 'tanker',
          label: "Citerne d'essence",
          count: 1,
          value: 100,
          rules: {
            replay: true,
          },
        },
        {
          name: 'sealant',
          label: 'Increvable',
          count: 1,
          value: 100,
          rules: {
            replay: true,
          },
        },
        {
          name: 'emergency',
          label: 'Véhicule prioritaire',
          count: 1,
          value: 100,
          rules: {
            replay: true,
          },
        },
      ],
    },
    {
      category: 'distance',
      types: [
        {
          name: 'km25',
          label: '25km',
          value: 25,
          count: 10,
        },
        {
          name: 'km50',
          label: '50km',
          value: 50,
          count: 10,
        },
        {
          name: 'km75',
          label: '75km',
          value: 75,
          count: 10,
        },
        {
          name: 'km100',
          label: '100km',
          value: 100,
          count: 12,
        },
        {
          name: 'km200',
          label: '200km',
          value: 200,
          count: 4,
          rules: {
            limit: 2,
          },
        },
      ],
    },
  ];

  return {
    definitions,
    initCards(cardsArray) {
      if (cardsArray) {
        cards = new CardDeck(cardsArray);
      } else {
        const newCardsArray = [];
        for (let i = 0; i < definitions.length; i += 1) {
          for (let j = 0; j < definitions[i].types.length; j += 1) {
            const card = {
              category: definitions[i].category,
              type: definitions[i].types[j].name,
              label: definitions[i].types[j].label,
              value: definitions[i].types[j].value,
              rules: definitions[i].types[j].rules,
            };
            for (let k = 0; k < definitions[i].types[j].count; k += 1) {
              card.id = `${definitions[i].types[j].name}_${k}`;
              newCardsArray.push(card);
            }
          }
        }
        cards = new CardDeck(newCardsArray);
        cards.shuffle();
      }

      return cards;
    },
    dealCards(nbCard = 1) {
      const cardHand = cards.draw(nbCard);

      return cardHand;
    },
    getCards() {
      return cards._stack;
    },
  };
};

export default angular.module('app.card', []).factory('cardService', CardService).name;
