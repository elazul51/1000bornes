import angular from 'angular';
import template from './game-menu.html';
import './game-menu.scss';

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
export class GameMenuComponent {
  static id = 'gameMenu';

  avatars = [
    'mario',
    'luigi',
    'yoshi',
    'peach',
    'bowser',
    'toad',
    'koopa',
    'wario',
    'rosalina',
    'morton-koopa-jr',
    'lakitu',
    'shy-guy',
    'toadette',
    'daisy',
    'ludwig-von-koopa',
    'donkey-kong',
    'lemmy-koopa',
    'waluigi',
    'wendy-o-koopa',
    'larry-koopa',
    'iggy-koopa',
    'isabelle',
    'roy-koopa',
    'link',
    'inkling-girl',
  ];
  playerAvatar = 'mario';
  apiPlayersEndpoint = '/api/v1/players';
  apiGamesEndpoint = '/api/v1/games';
  showJoinNameInput = true;
  showCreateNameInput = true;
  selectedGame = null;
  gamesLimit = 6;

  /* @ngInject */
  constructor($http, $scope, $state, $interval, moment, dialogService, socketService, cardService) {
    this.$http = $http;
    this.$state = $state;
    this.$interval = $interval;
    this.moment = moment;
    this.dialogService = dialogService;
    this.socketService = socketService;
    this.cardService = cardService;

    $scope.$on('$destroy', () => {
      socketService.unsyncUpdates('games');
    });
  }

  $onInit() {
    this.$http.get(this.apiGamesEndpoint)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to get games');
        }
        this.games = response.data;
        this.checkActiveGames();
        this.socketService.syncUpdates('games', this.games);
      })
      .catch((error) => {
        console.error('An error occured while getting games: ', error);
      });
  }

  deactivateGame(game) {
    this.$http.put(`${this.apiGamesEndpoint}/${game._id}`, {
      active: false,
    })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to update game');
        }
      })
      .catch((error) => {
        console.error('An error occured while updating the game: ', error);
      });
  }

  // Set active games not started to inactive after 5 minutes
  checkActiveGames() {
    for (let i = 0; i < this.games.length; i += 1) {
      const game = this.games[i];
      const now = this.moment.utc();
      if (game.active &&
        !game.started &&
        this.moment.duration(now.diff(game.updatedAt)).minutes() >= 5) {
        this.deactivateGame(game);
      }
    }
  }

  selectNbPlayer = (event, nbPlayer) => {
    angular.element(event.currentTarget).removeClass('active');
    this.nbPlayer = nbPlayer;
    this.showCreateNameInput = !this.showCreateNameInput;
  };

  selectPlayerAvatar = (avatar) => {
    this.playerAvatar = avatar;
  };

  selectGame = (game) => {
    this.selectedGame = game;
    this.showJoinNameInput = !this.showJoinNameInput;
  };

  hasWaitingGames = () => this.games.find(game => game.started === false);

  createGame = (nbPlayer, playerName) => {
    if (!(nbPlayer && playerName && this.games.length < this.gamesLimit)) {
      return;
    }

    this.cardService.initCards();
    this.$http.get(`${this.apiPlayersEndpoint}/name/${playerName}`)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to get player');
        }
        if (response.data) {
          this.dialogService.alert(`L'utilisateur ${playerName} existe déjà, veuillez saisir un autre pseudo`, 'Pseudo existant');
          throw new Error('Duplicate player name detected');
        }
        return this.$http.post(this.apiPlayersEndpoint, {
          name: playerName,
          avatar: this.playerAvatar,
          number: 1,
          cards: {
            hand: this.cardService.dealCards(6),
            discarded: [],
            km25: [],
            km50: [],
            km75: [],
            km100: [],
            km200: [],
            speed: [],
            battle: [],
            safety: [null, null, null, null],
          },
          turn: true,
        });
      })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to insert player');
        }
        this.player = response.data;
        return this.$http.post(this.apiGamesEndpoint, {
          active: true,
          started: false,
          nbPlayer,
          players: [this.player._id],
          cards: this.cardService.getCards(),
        });
      })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to insert game');
        }
        const game = response.data;
        this.socketService.socket.emit('sync-game', game);
        this.$state.go('game', { game, playerId: this.player._id });
      })
      .catch((error) => {
        console.error('An error occured while creating the game: ', error);
      });
  };

  joinGame = (playerName) => {
    if (!playerName) {
      return;
    }

    this.cardService.initCards(this.selectedGame.cards);
    this.$http.get(`${this.apiPlayersEndpoint}/name/${playerName}`)
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to get player');
        }
        if (response.data) {
          this.dialogService.alert(`L'utilisateur ${playerName} existe déjà, veuillez saisir un autre pseudo`, 'Pseudo existant');
          throw new Error('Duplicate player name detected');
        }
        return this.$http.post(this.apiPlayersEndpoint, {
          name: playerName,
          avatar: this.playerAvatar,
          number: this.selectedGame.players.length + 1,
          cards: {
            hand: this.cardService.dealCards(6),
            discarded: [],
            km25: [],
            km50: [],
            km75: [],
            km100: [],
            km200: [],
            speed: [],
            battle: [],
            safety: [null, null, null, null],
          },
        });
      })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to insert player');
        }
        this.player = response.data;
        this.selectedGame.players.push(this.player);
        const started = this.selectedGame.players.length === this.selectedGame.nbPlayer;
        const players = this.selectedGame.players.map(player => player._id);
        return this.$http.put(`${this.apiGamesEndpoint}/${this.selectedGame._id}`, {
          started,
          players,
          cards: this.cardService.getCards(),
        });
      })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Error: unable to update game');
        }
        const game = response.data;
        this.socketService.socket.emit('sync-game', game);
        this.$state.go('game', { game, playerId: this.player._id });
      })
      .catch((error) => {
        console.error('An error occured while joining the game: ', error);
      });
  };
}

export default angular
  .module('directives.game-menu', [])
  .component(GameMenuComponent.id, {
    template,
    controller: GameMenuComponent,
  }).name;
