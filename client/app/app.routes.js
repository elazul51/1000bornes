export default function routes($stateProvider, $urlRouterProvider) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $stateProvider.state('gameMenu', {
    url: '/',
    component: 'gameMenu',
  });

  $stateProvider.state('game', {
    url: '/game',
    params: { game: null, playerId: null },
    component: 'game',
  });
}
